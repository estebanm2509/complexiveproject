import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Promedio } from '../models-interfaces/promedios';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-average',
  templateUrl: './list-average.component.html',
  styleUrls: ['./list-average.component.css']
})
export class ListAverageComponent implements OnInit{
  
  
  body: any;
  IdUser: string | null;
  displayedColumns: string[] = ['nacionalidad'];


  constructor(
    private usersService: UsersService,
    private routeActivate: ActivatedRoute,
  ) { 
    /*this.buildForm();*/
    console.log(this.routeActivate.snapshot.paramMap.get('id'))
    this.IdUser = this.routeActivate.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getPromedio();
  }


  dataSource: Promedio[] = [];

  private getPromedio(): void {
    
    this.usersService.getAverageById(this.IdUser).subscribe(
      (resp: any) => {
        console.log(resp, "result");
        
        this.dataSource = resp.ctl.edadesPromedio[0];
        console.log(this.dataSource, "  PROMEDIOS");
        
        /* for (const key of this.dataSource) {
          console.log(key, "   key");
        } */
      }
    );
  }
  
}
