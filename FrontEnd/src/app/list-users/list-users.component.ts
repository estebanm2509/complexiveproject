import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Persona } from '../models-interfaces/getUsers';
import { NewPerson } from '../models-interfaces/postUsers';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  body: any;
  
  displayedColumns: string[] = ['code', 'name', 'lastname', 'birthday', "gender", "education", 'nationality'];
  dataSource: Persona[] = [];
  todayWithPipe = null
  today: Date = new Date();
  pipe = new DatePipe('en-US');

  constructor(
    private usersService: UsersService
  ) { }
  
  ngOnInit(): void {
    console.log("prueba");
    this.getUsers();
  }

  private getUsers(): void {
  
    this.body = {
      "ipUser": "10..11.20",
      "uuidUser": "cae93b1b-953c-4f4d-a8c0-894cc5c76956-3",
      "userApp": "USR_WEB",
      "channel": "WEB",
      "operation": "CRUD CLIENT",
      "ctl": { 
          "cNacionalidad" : ""
      }
  }


    this.usersService.getUsers(this.body).subscribe(
      (resp: any) => {
        console.log(resp, "  DATAUSERS");
        this.dataSource = resp.ctl.persona;
        
        console.log(this.dataSource[0].fechaNacimiento, '   MOSTRANDO FECHA DE NACIMIENTO')
        var fecha = this.dataSource[0].fechaNacimiento.toString
      }
    );
  }


  public manPerson(codigo: number) {
    const manPerson: NewPerson = {
      ipUser: "10..11.20",
      uuidUser: "cae93b1b-953c-4f4d-a8c0-894cc5c76956-3",
      userApp: "userone",
      channel: "WEB",
      operation: "CRUD CLIENT",
      ctl: {
        codigo: null,
        primerNombre: "Lucas",
        segundoNombre: "Benito",
        apellidoPaterno: "Morocho",
        apellidoMaterno: "Alvarado",
        fechaNacimiento: "10/05/2015",
        sexo: "HOMBRE",
        cNivelEducacion: "",
        cNacionalidad: "10"
      }
    };
    
    this.usersService.postUsers(manPerson).subscribe(
      (resp: any) => {
        console.log(resp, '    MANTENIMIENTO GET')
      }
    );
    console.log(codigo, "---codigo----");

  }
}
