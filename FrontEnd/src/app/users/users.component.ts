import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GetUserById } from '../models-interfaces/getUserById';
import { NewPerson } from '../models-interfaces/postUsers';
import { ListNacionalidades, Ctl } from '../models-interfaces/nacionalidades';
import { ListLevelEducation } from '../models-interfaces/nivelEducacion';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

 
  dataCountryAll!: [];
  dataLevelEducationAll!: [];

  public formGroup!: FormGroup;
  IdUser: string | null;
  valorSearch: string;
  bodyContainer: GetUserById | undefined;
  titleEmployee = 'Crear Empleado';
  
  constructor( private formBuilder: FormBuilder, 
    private usersService: UsersService,
    private routeActivate: ActivatedRoute,
    private router: Router) { 
    this.valorSearch = "";
    /*this.buildForm();*/
    console.log(this.routeActivate.snapshot.paramMap.get('id'))
    this.IdUser = this.routeActivate.snapshot.paramMap.get('id');


    this.formGroup = this.formBuilder.group({
      primerNombre: [''],
      segundoNombre: [''],
      apellidoPaterno: [''],
      apellidoMaterno: [''],
      fechaNacimiento: [''],
      sexo: [''],
      cNivelEducacion: [''],
      cNacionalidad: [''],
    });


    
  }
  
  ngOnInit(): void {
    this.getNacionalidades();
    this.getUserById();
    this.getLevelsEducation();
    /* this.formGroup.setValue({
      ipUser: "asad"
    }) */
  }

  private buildForm() {
    this.formGroup = this.formBuilder.group({
      ipUser: [""],
    });
  }

  save(event: Event) {
    event.preventDefault();
    
    const manPerson: NewPerson = {
      ipUser: "10..11.20",
      uuidUser: "cae93b1b-953c-4f4d-a8c0-894cc5c76956-3",
      userApp: "userone",
      channel: "WEB",
      operation: "CRUD CLIENT",
      ctl: {
        codigo: null,
        primerNombre: this.formGroup.value.primerNombre,
        segundoNombre: this.formGroup.value.segundoNombre,
        apellidoPaterno: this.formGroup.value.apellidoPaterno,
        apellidoMaterno: this.formGroup.value.apellidoMaterno,
        fechaNacimiento: this.formGroup.value.fechaNacimiento,
        sexo: this.formGroup.value.sexo,
        cNivelEducacion: this.formGroup.value.cNivelEducacion,
        cNacionalidad: this.formGroup.value.cNacionalidad.toString()
      }
    };

    
    console.log(manPerson, "-----save----");
    this.usersService.postUsers(manPerson).subscribe(
      (resp: any) => {
        console.log(resp, '    MANTENIMIENTO GET')
      }
    );

    this.router.navigate(['/employees-list']);
    
  }


  private getUserById(): void {
    if (this.IdUser !== null) {
      console.log("EDITAR");
      const manPerson: GetUserById = {
        ipUser: "10.1.11.20",
        uuidUser: "cae93b1b-953c-4f4d-a8c0-894cc5c76956-3",
        userApp: "userone",
        channel: "WEB",
        operation: "CRUD CLIENT",
        ctl: {
          cPersona: ""
        }
      };
      manPerson.ctl.cPersona = this.IdUser;
      this.usersService.getUserById(manPerson).subscribe(
        (resp: any) => {
          console.log(resp, '    DATA GET')
        }
      );
      
    }
    console.log("NUEVO");
    
  }

  private getNacionalidades(): void {
    const nacionalidades: ListNacionalidades = {
      ipUser: "10.1.11.20",
      uuidUser: "cae93b1b-953c-4f4d-a8c0-894cc5c76956-3",
      userApp: "userone",
      channel: "WEB",
      operation: "CRUD CLIENT",
      ctl: {
        cNacionalidad: ""
      }
    };

    this.usersService.getNacionalidades(nacionalidades).subscribe(
      (resp: any) => {
        console.log(resp, '    NACIONALIDADES')
        this.dataCountryAll = resp.ctl.nacionalidad.map((item: any) => {
          console.log(item)
          return item.isonacionalidad;
        })
      }
    );
  }

  private getLevelsEducation(): void {
    const levelEducation: ListLevelEducation = {
      ipUser: "10.1.11.20",
      uuidUser: "cae93b1b-953c-4f4d-a8c0-894cc5c76956-3",
      userApp: "userone",
      channel: "WEB",
      operation: "CRUD CLIENT",
      ctl: {
        cNivelEducacion: ""
      }
    };

    this.usersService.getLevelsEducation(levelEducation).subscribe(
      (resp: any) => {
        console.log(resp, '    NIVEL DE EDUCACION')
        this.dataLevelEducationAll = resp.ctl.nivelEducacion.map((item: any) => {
          console.log(item)
          return item.sigla;
        })
      }
    );
  }

}
