export interface Promedio {
    ipUser: string;
    uuidUser: string;
    userApp: string;
    channel: string;
    operation: string;
    ctl: CTLAverage;
}


export interface CTLAverage {
    cNacionalidad: string | null;
}
