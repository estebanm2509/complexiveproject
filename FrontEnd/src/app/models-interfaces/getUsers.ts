export interface GetUsersI {
    ipUser: string;
    uuidUser: string;
    userApp: string;
    channel: string;
    operation: string;
    ctl: Ctl[];
}

export interface Ctl {
    cPersona: string;
}

export interface GetUsersResponse {
    code: string;
    usr: string;
    dateTrn: string;
    uuidTrn: string;
    msgUser: string;
    msgTech: string;
    ctl: CtlResponse[];
}

export interface CtlResponse {
    persona: Persona[];
}

export interface Persona {
    codigo: number;
    primerNombre: string;
    segundoNombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    fechaNacimiento: string;
    sexo: string;
    nivelEducacion: NivelEducacion[];
    nacionalidad: Nacionalidad[];
}

export interface NivelEducacion {
    codigo: number;
    sigla: string;
    descripcion: string;
}

export interface Nacionalidad {
    codigo: number;
    paisNacionalidad: string;
    genticilioNacionalidad: string;
    isonacionalidad: string;
}