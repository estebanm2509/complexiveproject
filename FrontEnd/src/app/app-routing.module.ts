import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListUsersComponent } from './list-users/list-users.component';
import { UsersComponent } from './users/users.component';
import { ListAverageComponent } from './list-average/list-average.component';

const routes: Routes = [
  { path: 'users-list', component: ListUsersComponent },
  { path: 'average-list', component: ListAverageComponent },
  { path: 'average-list/:id', component: ListAverageComponent },
  { path: 'users-create', component: UsersComponent },
  { path: 'users-edit/:id', component: UsersComponent },
  { path: '', redirectTo: 'users-list', pathMatch: 'full' },
  { path: '**', redirectTo: 'users-list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
